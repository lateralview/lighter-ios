//
//  WelcomeViewController.m
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/15/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import "WelcomeViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "MBProgressHUD.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)facebookAction:(id)sender{
    
    NSArray *permissions = @[@"public_profile", @"email"];
    [PFFacebookUtils logInWithPermissions:permissions block:^(PFUser *user, NSError *error) {
        if (!user) {
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
        } else if (user.isNew) {
            NSLog(@"User with facebook signed up and logged in!");
            
            FBRequest *request = [FBRequest requestForMe];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [request startWithCompletionHandler:^(FBRequestConnection *connection,
                                                  id result,
                                                  NSError *error) {
                if (!error) {
                    DLog(@"Result: %@", result);
                    NSString *facebookUsername = [result objectForKey:@"username"];
                    NSString *fbid = [result objectForKey:@"id"];
                    NSString *facebookFirstName = [result objectForKey:@"first_name"];
                    NSString *facebookLastName = [result objectForKey:@"last_name"];
                    NSString *facebookAvatarUrl = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=240&height=240", fbid];
                    
                    user[@"fullname"] = [NSString stringWithFormat:@"%@ %@",facebookFirstName,facebookLastName];
                    
                    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:facebookAvatarUrl]];
                    PFFile *imageFile = [PFFile fileWithName:[NSString stringWithFormat:@"%@-avatar.png",user.username] data:imageData];
                    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                        if (!error) {
                            [user setObject:imageFile forKey:@"avatar"];
                            [user saveInBackground];
                        }else{
                            NSString *errorString = @"Check your network connection.";
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:errorString delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                            [alert show];
                        }
                    }];
                    
                }else{
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                }
            }];
            
            [self goToDashboard];
        } else {
            [self goToDashboard];
        }
    }];
}

- (void)goToDashboard{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *initView = [storyboard instantiateInitialViewController];
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = initView;
}

@end

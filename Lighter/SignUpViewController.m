//
//  SignUpViewController.m
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/15/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import "SignUpViewController.h"
#import "MBProgressHUD.h"

@interface SignUpViewController (){
    IBOutlet UITextField *usernameTxt;
    IBOutlet UITextField *passwordTxt;
    IBOutlet UITextField *emailTxt;
    IBOutlet UITextField *fullnameTxt;
}

@end

@implementation SignUpViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    [UIView animateWithDuration:0.3 animations:^{
        self.navigationController.navigationBarHidden = NO;
    } completion:^(BOOL finished) {
        self.title = @"Registrarse";
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signUpAction{
    if([self isValid]) {
        PFUser *user = [PFUser user];
        user.username = usernameTxt.text;
        user.password = passwordTxt.text;
        user.email = emailTxt.text;
        user[@"fullname"] = fullnameTxt.text;
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (!error) {
                // Hooray! Let them use the app now.
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *initView = [storyboard instantiateInitialViewController];
                
                UIWindow* window = [[UIApplication sharedApplication] keyWindow];
                window.rootViewController = initView;
                
            } else {
                NSString *errorString = [error userInfo][@"error"];
                // Show the errorString somewhere and let the user try again.
                NSLog(@"Error: %@", errorString);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:errorString delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                [alert show];
            }
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Complete todos datos" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
        [alert show];
    }
}

- (BOOL)isValid{
    
    if ([usernameTxt.text isEqualToString:@""]) {
        return NO;
    }
    if ([passwordTxt.text isEqualToString:@""]) {
        return NO;
    }
    if ([emailTxt.text isEqualToString:@""]) {
        return NO;
    }
    if ([fullnameTxt.text isEqualToString:@""]) {
        return NO;
    }
    
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10.0;
}

@end

//
//  LikeCell.m
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/24/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import "LikeCell.h"

@implementation LikeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

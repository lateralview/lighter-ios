//
//  SettingsViewController.m
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/15/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController () {
    IBOutlet UILabel *appVersionLbl;
}

@end

@implementation SettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor colorWithHex:@"F5F5F5" alpha:1.0];
    
    NSString *appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString *appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    appVersionLbl.text = [NSString stringWithFormat:@"v%@ %@", appVersionString, appBuildString];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logoutAction:(id)sender{
    [PFUser logOut];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Welcome" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"WelcomeNav"];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    window.rootViewController = vc;
}

@end

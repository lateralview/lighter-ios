//
//  ShowsViewController.m
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/15/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import "ShowsViewController.h"
#import "ShowCell.h"
#import "ShowProfileViewController.h"

@interface ShowsViewController () <ShowCellDelegate> {
    NSMutableArray *listOfElements;
    IBOutlet UITableView *table;
    IBOutlet UIButton *avatarBut;
}

@end

@implementation ShowsViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    avatarBut.layer.cornerRadius = 15;
    avatarBut.layer.masksToBounds = YES;
    avatarBut.layer.borderColor = [UIColor whiteColor].CGColor;
    avatarBut.layer.borderWidth = 1;
    
    PFFile *avatar = [PFUser currentUser][@"avatar"];
    [avatarBut loadImageFromURL:avatar.url placeholderImage:[UIImage imageNamed:@"avatar"]];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Show"];
    [query includeKey:@"artist"];
    [query orderByAscending:@"date"];
    [query setCachePolicy:kPFCachePolicyCacheThenNetwork];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            listOfElements = [objects mutableCopy];
            [table reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 240;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listOfElements count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShowCell" forIndexPath:indexPath];
    cell.delegate = self;
    
    PFObject *obj = [listOfElements objectAtIndex:indexPath.row];
    PFFile *picture = obj[@"picture"];
    [cell.pictureView loadImageFromURL:picture.url placeholderImage:nil];
    
    cell.titleLbl.text = obj[@"artist"][@"name"];
    cell.placeLbl.text = obj[@"venue"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE dd <> MMMM <> YYYY"];
    NSString *dateStr = [formatter stringFromDate:obj[@"date"]];
    dateStr = [dateStr capitalizedString];
    cell.dateLbl.text = [dateStr stringByReplacingOccurrencesOfString:@"<>" withString:@"de"];
    
    cell.priceLbl.text = [NSString stringWithFormat:@"$%@", obj[@"price"]];
    cell.contView.layer.masksToBounds = YES;
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // Get visible cells on table view.
    NSArray *visibleCells = [table visibleCells];
    
    for (ShowCell *cell in visibleCells) {
        [cell cellOnTableView:table didScrollOnView:self.view];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"gotoShowProfile"]) {
        ShowProfileViewController *vc = [segue destinationViewController];
        PFObject *show = [listOfElements objectAtIndex:table.indexPathForSelectedRow.row];
        vc.show = show;
    }
}

#pragma mark - Show Cell

- (void)didLikeCell:(int)row{
    PFObject *show = [listOfElements objectAtIndex:row];
    
    PFUser *user = [PFUser currentUser];
    PFRelation *relation = [user relationForKey:@"attends"];
    [relation addObject:show];
    [user saveInBackground];
}

@end

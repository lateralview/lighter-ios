//
//  ShowCell.h
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/15/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShowCellDelegate <NSObject>

- (void)didLikeCell:(int)row;

@end

@interface ShowCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *pictureView;
@property (nonatomic, weak) IBOutlet UIView *contView;
@property (nonatomic, weak) IBOutlet UILabel *titleLbl;
@property (nonatomic, weak) IBOutlet UILabel *placeLbl;
@property (nonatomic, weak) IBOutlet UILabel *dateLbl;
@property (nonatomic, weak) IBOutlet UILabel *priceLbl;
@property (nonatomic, assign) int row;

- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view;

@property (nonatomic, weak) id<ShowCellDelegate> delegate;

@end

//
//  AppDelegate.m
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/14/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import <FacebookSDK/FacebookSDK.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [Parse setApplicationId:@"WYjhFf5bjvY1oeHlfCqcabmwZC8vbMqONLlUpCGt"
                  clientKey:@"bTWao6tohoXiypnp3DHmu88v6RDeMZ2h61ftDDgZ"];
    [PFFacebookUtils initializeFacebook];
    
    [[Mask shared]setUp];
    
    [self applyStylesheet];
    
    if(![PFUser currentUser]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Welcome" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"WelcomeNav"];
        self.window.rootViewController = vc;
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:[PFFacebookUtils session]];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)applyStylesheet{
    
    UINavigationBar *navigationBar = [UINavigationBar appearance];
    [navigationBar setTintColor:[UIColor colorWithHex:@"FFFFFF" alpha:1.0]];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [navigationBar setBarTintColor:[UIColor colorWithHex:@"E1283B" alpha:1.0]];
        
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    UIBarButtonItem *barButton = [UIBarButtonItem appearance];
    barButton.tintColor = [UIColor colorWithHex:@"FFFFFF" alpha:1.0];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor colorWithHex:@"FFFFFF" alpha:1.0],NSForegroundColorAttributeName,
                                               [UIFont fontWithName:@"Avenir-Medium" size:22.0], NSFontAttributeName,nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
}

@end

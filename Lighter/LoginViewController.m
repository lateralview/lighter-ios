//
//  LoginViewController.m
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/15/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import "LoginViewController.h"
#import "MBProgressHUD.h"

@interface LoginViewController (){
    IBOutlet UITextField *usernameTxt;
    IBOutlet UITextField *passwordTxt;
}

@end

@implementation LoginViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"";

    self.tableView.backgroundColor = [UIColor colorWithHex:@"F5F5F5" alpha:1.0];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated{
    [UIView animateWithDuration:0.3 animations:^{
        self.navigationController.navigationBarHidden = NO;
    } completion:^(BOOL finished) {
        self.title = @"Iniciar sesión";
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)loginAction:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [PFUser logInWithUsernameInBackground:usernameTxt.text
                                 password:passwordTxt.text
                                    block:^(PFUser *user, NSError *error) {
                                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                                        if (user) {
                                            
                                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                            UIViewController *initView = [storyboard instantiateInitialViewController];
                                            
                                            UIWindow* window = [[UIApplication sharedApplication] keyWindow];
                                            window.rootViewController = initView;
                                            
                                        } else {
                                            
                                            // The login failed. Check error to see why.
                                            NSLog(@"%@", error.description);
                                            
                                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Usuario o Contraseña invalida." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                                            [alert show];
                                            
                                        }
                                    }];
}

- (IBAction)forgotPassAction:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Olvide la contraseña" message:@"Ingrese su email para recuperar su contraseña:" delegate:nil cancelButtonTitle:@"Enviar" otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *textField = [alert textFieldAtIndex:0];
    textField.keyboardType = UIKeyboardTypeEmailAddress;
    [alert showWithBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        UITextField *textField = [alertView textFieldAtIndex:0];
        if (![textField.text isEqualToString:@""]) {
            [PFUser requestPasswordResetForEmailInBackground:textField.text];
            
            UIAlertView *confirmMessage = [[UIAlertView alloc]initWithTitle:@"" message:@"Revise su correo electronico para eleguir una nueva contraseña." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [confirmMessage show];
        }
        
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10.0;
}

@end

//
//  ProfileViewController.m
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/15/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import "ProfileViewController.h"
#import "LikeCell.h"

@interface ProfileViewController (){
    IBOutlet UIImageView *avatarView;
    IBOutlet UILabel *nameLbl;
    IBOutlet UIView *headerView;
    IBOutlet UIView *headerUpView;
    
    NSMutableArray *listOfLikes;
}

@end

@implementation ProfileViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    avatarView.layer.cornerRadius = 60;
    avatarView.layer.masksToBounds = YES;
    avatarView.layer.borderColor = [UIColor whiteColor].CGColor;
    avatarView.layer.borderWidth = 2;
    PFFile *avatar = [PFUser currentUser][@"avatar"];
    [avatarView loadImageFromURL:avatar.url placeholderImage:[UIImage imageNamed:@"avatar"]];
    
    nameLbl.text = [PFUser currentUser][@"fullname"];
    
    headerView.backgroundColor = [UIColor colorWithHex:@"E1283B" alpha:1.0];
    headerUpView.backgroundColor = [UIColor colorWithHex:@"E1283B" alpha:1.0];
    self.view.backgroundColor = [UIColor colorWithHex:@"E1283B" alpha:1.0];
    self.tableView.backgroundColor = [UIColor colorWithHex:@"F5F5F5" alpha:1.0];
    
    PFUser *user = [PFUser currentUser];
    PFRelation *relation = [user relationForKey:@"attends"];
    [[relation query]includeKey:@"artist"];
    [[relation query] setCachePolicy:kPFCachePolicyCacheThenNetwork];
    [[relation query] findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            listOfLikes = [objects mutableCopy];
            [self.tableView reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listOfLikes count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Favoritos";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LikeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LikeCell" forIndexPath:indexPath];
    
    PFObject *fav = [listOfLikes objectAtIndex:indexPath.row];
    
    PFObject *artist = fav[@"artist"];
    [artist fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        cell.nameLbl.text = artist[@"name"];
    }];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE dd <> MMMM <> YYYY"];
    NSString *dateStr = [formatter stringFromDate:fav[@"date"]];
    dateStr = [dateStr capitalizedString];
    cell.dateLbl.text = [dateStr stringByReplacingOccurrencesOfString:@"<>" withString:@"de"];
    
    return cell;
}

@end

//
//  ShowProfileViewController.m
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/24/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import "ShowProfileViewController.h"
#import <MapKit/MapKit.h>

@interface ShowProfileViewController () {
    IBOutlet UIImageView *pictureView;
    IBOutlet UIView *contView;
    IBOutlet UILabel *titleLbl;
    IBOutlet UILabel *descriptionLbl;
    IBOutlet UILabel *venueLbl;
    IBOutlet UILabel *addressLbl;
    IBOutlet UILabel *cityLbl;
    IBOutlet UILabel *dateLbl;
    IBOutlet UILabel *priceLbl;
    IBOutlet MKMapView *venueView;
    IBOutlet UIButton *ticketBut;
}

@end

@implementation ShowProfileViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    PFFile *picture = _show[@"picture"];
    [pictureView loadImageFromURL:picture.url placeholderImage:nil];
    
    titleLbl.text = _show[@"artist"][@"name"];
    descriptionLbl.text = _show[@"description"];
    
    venueLbl.text = _show[@"venue"];
    addressLbl.text = _show[@"address"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE dd <> MMMM <> YYYY"];
    NSString *dateStr = [formatter stringFromDate:_show[@"date"]];
    dateStr = [dateStr capitalizedString];
    dateLbl.text = [dateStr stringByReplacingOccurrencesOfString:@"<>" withString:@"de"];
    
    [formatter setDateFormat:@"EEE dd <> MMM"];
    dateStr = [formatter stringFromDate:_show[@"date"]];
    dateStr = [dateStr capitalizedString];
    self.title = [dateStr stringByReplacingOccurrencesOfString:@"<>" withString:@"de"];
    
    venueView.showsUserLocation = NO;
    NSString *location = _show[@"address"];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:location
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     if (placemarks && placemarks.count > 0) {
                         CLPlacemark *topResult = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                         
                         MKCoordinateRegion region = venueView.region;
                         region.center = placemark.region.center;
                         region.span.longitudeDelta /= 500.0;
                         region.span.latitudeDelta /= 500.0;
                         
                         [venueView setRegion:region animated:YES];
                         [venueView addAnnotation:placemark];
                     }
                 }
     ];
    
    if ([_show[@"ticket_url"]isEqualToString:@""]) {
        ticketBut.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)gotoTicket:(id)sender{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:_show[@"ticket_url"]]];
}

@end

//
//  UIAlertView+Extended.m
//  LvKit
//
//  Created by Leandro on 1/10/14.
//  Copyright (c) 2014 Lateral View. All rights reserved.
//

#import "UIAlertView+Extended.h"
#import <objc/runtime.h>

@interface LVAlertWrapper : NSObject <UIAlertViewDelegate>

@property (copy) void(^completionBlock)(UIAlertView *alertView, NSInteger buttonIndex);

@end

@implementation LVAlertWrapper

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.completionBlock)
        self.completionBlock(alertView, buttonIndex);
}

- (void)alertViewCancel:(UIAlertView *)alertView
{
    if (self.completionBlock)
        self.completionBlock(alertView, alertView.cancelButtonIndex);
}

@end


static const char kLVAlertWrapper;

@implementation UIAlertView (Extended)

- (void)showWithBlock:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion
{
    LVAlertWrapper *alertWrapper = [[LVAlertWrapper alloc] init];
    alertWrapper.completionBlock = completion;
    self.delegate = alertWrapper;
    
    objc_setAssociatedObject(self, &kLVAlertWrapper, alertWrapper, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [self show];
}


@end

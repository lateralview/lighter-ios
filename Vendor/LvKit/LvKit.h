//
//  LvKit.h
//  LvKit
//
//  Created by Leandro on 1/10/14.
//  Copyright (c) 2014 Lateral View. All rights reserved.
//

#ifndef LvKit_LvKit_h
#define LvKit_LvKit_h

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define IS_4_INCH (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

#define IS_IPHONE [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone
#define IS_IPAD !(IS_IPHONE)
#define DEVICE_NAME ((IS_IPHONE) ? @"iPhone" : @"iPad")

#import "LvCache.h"
#import "UIAlertView+Extended.h"
#import "UIColor+Extended.h"
#import "UIImageView+Extended.h"
#import "UIView+Extended.h"
#import "UIButton+Extended.h"

#endif

//
//  UIButton+Extended.m
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/24/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import "UIButton+Extended.h"
#import <objc/runtime.h>
#import "LvCache.h"

static char URLKEY;

@implementation UIButton (Extended)

@dynamic imageURL;

- (void)loadImageFromURL:(NSString*)urlStr placeholderImage:(UIImage*)placeholder{
    
    NSData *cachedData = [LvCache objectForKey:urlStr];
	if (cachedData) {
        self.imageURL = nil;
        [self setImage:[UIImage imageWithData:cachedData] forState:UIControlStateNormal];
        return;
	}else{
        NSURL *url = [NSURL URLWithString:urlStr];
        self.imageURL = url;
        [self setImage:placeholder forState:UIControlStateNormal];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage *imageFromData = [UIImage imageWithData:data];
            [LvCache setObject:data forKey:urlStr];
            if (imageFromData) {
                if ([self.imageURL.absoluteString isEqualToString:url.absoluteString]) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [UIView transitionWithView:self
                                          duration:0.5f
                                           options:UIViewAnimationOptionTransitionCrossDissolve
                                        animations:^{
                                            [self setImage:imageFromData forState:UIControlStateNormal];
                                        } completion:nil];
                    });
                }
            }
            self.imageURL = nil;
        });
    }
}

- (void)setImageURL:(NSURL *)newImageURL {
	objc_setAssociatedObject(self, &URLKEY, newImageURL, OBJC_ASSOCIATION_COPY);
}

- (NSURL*)imageURL {
	return objc_getAssociatedObject(self, &URLKEY);
}

@end

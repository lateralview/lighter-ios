//
//  UIView+Extended.m
//  LvKit
//
//  Created by Leandro on 1/10/14.
//  Copyright (c) 2014 Lateral View. All rights reserved.
//

#import "UIView+Extended.h"

@implementation UIView (Extended)

- (void)makeCircular {
    CGFloat diameter = self.frame.size.width;
    CGPoint saveCenter = self.center;
    CGRect newFrame = CGRectMake(self.frame.origin.x, self.frame.origin.y, diameter, diameter);
    self.frame = newFrame;
    self.layer.cornerRadius = diameter / 2.0;
    self.center = saveCenter;
}

@end

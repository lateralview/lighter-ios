//
//  MicroStore.m
//  Attender
//
//  Created by Juan Manuel Abrigo on 9/22/13.
//  Copyright (c) 2013 Lateral View. All rights reserved.
//

#import "MicroStore.h"

@implementation MicroStore

+ (MicroStore *)shared
{
    static MicroStore *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[MicroStore alloc] init];
    });
    return shared;
}

- (void)saveObject:(id)object WithKey:(NSString*)key{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.micro", key]];
    
    [NSKeyedArchiver archiveRootObject:object toFile:path];
}

- (id)retrieveObjectWithKey:(NSString*)key{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.micro", key]];
    
    id object = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    
    return object;
}

@end

//
//  MicroStore.h
//  Attender
//
//  Created by Juan Manuel Abrigo on 9/22/13.
//  Copyright (c) 2013 Lateral View. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MicroStore : NSObject

+ (MicroStore *)shared;

- (void)saveObject:(id)object WithKey:(NSString*)key;
- (id)retrieveObjectWithKey:(NSString*)key;

@end

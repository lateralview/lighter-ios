//
//  UIViewController+EmptyBackButton.h
//  Lighter
//
//  Created by Juan Manuel Abrigo on 8/24/14.
//  Copyright (c) 2014 Lateral View LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (EmptyBackButton)

@end
